// HOME -> certificacoes
$(document).ready(function(){
	$('.carousel-certificacoes').slick({
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 1,
		variableWidth: true,
		autoplay: true,
		autoplaySpeed: 3000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 4,
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 4,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
				variableWidth: false,
			}
		}
		]
	});
});

// SOBRE -> historia
$(document).ready(function(){
	$('.carousel-fotos-descricao').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000,
		fade: true,
		asNavFor: '.slider-nav'
	});

	$('.slider-nav').slick({
		slidesToShow: 5,
	  	slidesToScroll: 1,
	  	asNavFor: '.carousel-fotos-descricao',
	  	dots: false,
	  	arrows: true,
	  	prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
	  	centerMode: false,
	  	focusOnSelect: true,
	  	responsive: [
	  	{
	  		breakpoint: 1025,
	  		settings: {
	  			slidesToShow: 4,
	  		}
	  	}, {
	  		breakpoint: 769,
	  		settings: {
	  			slidesToShow: 3,
	  		}
	  	}, {
	  		breakpoint: 520,
	  		settings: {
	  			slidesToShow: 2,
	  		}
	  	}
	  	]
	});
});

// EXTRATO LIQUIDO -> outros produtos
$(document).ready(function(){
	$('.carousel-outros-produtos').slick({
		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		variableWidth: false,
		autoplay: true,
		autoplaySpeed: 4000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 1,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
			}
		}
		]
	});
});

// A GRANEL -> lista produtos
$(document).ready(function(){
	$('.carousel-lista-produtos').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		variableWidth: false,
		autoplay: true,
		autoplaySpeed: 4000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 4,
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 2,
			}
		}
		]
	});
});

//Iniciar animações
AOS.init({
	disable: 'mobile',
	delay: 0,
	duration: 1200,
	once: true,
});